package fi.maas.whim.models

import com.google.gson.annotations.SerializedName

data class GoogleDirectionResponse(
    @SerializedName("geocoder_status") val geocoded_waypoints: List<GeocodedWaypoint>,
    @SerializedName("routes") val routes: List<Route>,
    @SerializedName("status") val status: String
)