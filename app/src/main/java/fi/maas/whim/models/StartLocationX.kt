package fi.maas.whim.models

data class StartLocationX(
    val lat: Double,
    val lng: Double
)