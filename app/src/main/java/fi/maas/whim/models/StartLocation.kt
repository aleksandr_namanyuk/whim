package fi.maas.whim.models

data class StartLocation(
    val lat: Double,
    val lng: Double
)