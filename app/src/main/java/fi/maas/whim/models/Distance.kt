package fi.maas.whim.models

data class Distance(
    val text: String,
    val value: Int
)