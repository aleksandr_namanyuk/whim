package fi.maas.whim.models

data class OverviewPolyline(
    val points: String
)