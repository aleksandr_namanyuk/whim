package fi.maas.whim.models

data class Bounds(
    val northeast: Northeast,
    val southwest: Southwest
)