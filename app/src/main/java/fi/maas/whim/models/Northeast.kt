package fi.maas.whim.models

data class Northeast(
    val lat: Double,
    val lng: Double
)