package fi.maas.whim.models

data class Southwest(
    val lat: Double,
    val lng: Double
)