package fi.maas.whim.models

data class DistanceX(
    val text: String,
    val value: Int
)