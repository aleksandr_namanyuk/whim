package fi.maas.whim.models

import com.google.gson.annotations.SerializedName

data class QueryRequest<T>(
    @SerializedName("batchcomplete") val batchComplete: String,
    @SerializedName("query") val query: T
)

data class GeosearchQuery(
    @SerializedName("geosearch") val geosearch: List<WikiPage>
)

data class WikiPage(
    @SerializedName("pageid") val pageId: String,
    @SerializedName("ns") val ns: Int,
    @SerializedName("title") val title: String,
    @SerializedName("lat") val lat: Double,
    @SerializedName("lon") val lon: Double,
    @SerializedName("dist") val distance: Double,
    @SerializedName("primary") val primary: String
)

data class PageDetailsQuery(
    @SerializedName("pages") val pages: HashMap<String, PageDetails>
)

data class PageDetails(
    @SerializedName("pageid") val pageId: Int,
    @SerializedName("ns") val ns: Int,
    @SerializedName("title") val title: String,
    @SerializedName("contentmodel") val contentmodel: String,
    @SerializedName("pagelanguage") val pagelanguage: String,
    @SerializedName("pagelanguagehtmlcode") val pagelanguagehtmlcode: String,
    @SerializedName("pagelanguagedir") val pagelanguagedir: String,
    @SerializedName("touched") val touched: String,
    @SerializedName("lastrevid") val lastrevid: Int,
    @SerializedName("length") val length: Int,
    @SerializedName("description") val description: String,
    @SerializedName("descriptionsource") val descriptionsource: String,
    @SerializedName("images") val images: List<WikiImage>,
    @SerializedName("coordinates") val coordinates: List<Coordinate>
)

data class WikiImage(
    @SerializedName("ns") val ns: Int,
    @SerializedName("title") val title: String
)

data class Coordinate(
    @SerializedName("lat") val lat: Double,
    @SerializedName("lon") val lon: Double
)

data class ImageInfoQuery(
    @SerializedName("pages") val pages: HashMap<String, ImageInfoPage>
)

data class ImageInfoPage(
    @SerializedName("ns") val ns: Int,
    @SerializedName("title") val title: String,
    @SerializedName("imageinfo") val imageinfo: List<ImageInfo>
)

data class ImageInfo(
    @SerializedName("url") val url: String,
    @SerializedName("descriptionurl") val descriptionurl: String,
    @SerializedName("descriptionshorturl") val descriptionshorturl: String
)