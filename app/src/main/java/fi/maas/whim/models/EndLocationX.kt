package fi.maas.whim.models

data class EndLocationX(
    val lat: Double,
    val lng: Double
)