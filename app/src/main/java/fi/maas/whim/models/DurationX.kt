package fi.maas.whim.models

data class DurationX(
    val text: String,
    val value: Int
)