package fi.maas.whim.models

data class EndLocation(
    val lat: Double,
    val lng: Double
)