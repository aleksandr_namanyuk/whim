package fi.maas.whim.models

data class Polyline(
    val points: String
)