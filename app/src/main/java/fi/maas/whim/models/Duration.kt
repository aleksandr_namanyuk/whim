package fi.maas.whim.models

data class Duration(
    val text: String,
    val value: Int
)