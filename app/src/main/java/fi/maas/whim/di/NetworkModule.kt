package fi.maas.whim.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fi.maas.whim.BuildConfig
import fi.maas.whim.core.WhimApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { provideApiService(get()) }
    single { provideGson() }
    single { provideRetrofit(get(), get()) }
    single { provideOkHttpClient() }
}

fun provideOkHttpClient(): OkHttpClient {
    val builder = OkHttpClient.Builder()
    builder.connectTimeout(30L, TimeUnit.SECONDS)
    builder.readTimeout(30L, TimeUnit.SECONDS)
    builder.writeTimeout(30L, TimeUnit.SECONDS)

    if(BuildConfig.ENABLE_NETWORK_LOGGING){
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        builder.addInterceptor(httpLoggingInterceptor)
    }

    return builder.build()
}

fun provideGson(): Gson =
    GsonBuilder()
        .create()

fun provideRetrofit(okHttpClient: OkHttpClient,
                    gson: Gson
) : Retrofit =
    Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .build()

fun provideApiService(retrofit: Retrofit) : WhimApi =
    retrofit.create(WhimApi::class.java)