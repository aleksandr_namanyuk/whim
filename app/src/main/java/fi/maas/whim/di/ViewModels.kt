package fi.maas.whim.di

import fi.maas.whim.R
import fi.maas.whim.presentation.MainPresenter
import fi.maas.whim.repository.GoogleDirectionRepository
import fi.maas.whim.repository.IGoogleDirectionRepository
import fi.maas.whim.viewmodels.MainViewModel
import fi.maas.whim.viewmodels.MarkerDetailsViewModel
import fi.maas.whim.repository.WikiPageRepository
import fi.maas.whim.utils.ImagesRequestUtils
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Main Activity Module
 */
val mainModule = module {
    single { WikiPageRepository(get()) }

    single<IGoogleDirectionRepository> {
        GoogleDirectionRepository(
            androidContext().getString(R.string.google_maps_key),
            get(),
            get()
        )
    }

    single { MainPresenter(get(), get(), get()) }

    viewModel { MainViewModel(get(), get()) }
}

val markerDetailsModule = module{
    viewModel { MarkerDetailsViewModel(get()) }
}

val utilsModule = module{
    single { ImagesRequestUtils() }
}

val appModules = listOf(networkModule, mainModule, utilsModule, markerDetailsModule)