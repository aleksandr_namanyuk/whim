package fi.maas.whim

import android.app.Application
import fi.maas.whim.core.log.WhimLog
import fi.maas.whim.di.appModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

@Suppress("unused")
class WhimApp : Application(){

    override fun onCreate() {
        super.onCreate()

        WhimLog.init()

        Timber.v("onCreate whim application")

        initKoin()
    }

    private fun initKoin(){
        startKoin {
            androidLogger()
            androidContext(this@WhimApp)
            modules(appModules)
        }
    }
}