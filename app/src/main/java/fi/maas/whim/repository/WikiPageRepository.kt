package fi.maas.whim.repository

import fi.maas.whim.core.WhimApi

class WikiPageRepository(private val api: WhimApi){

    fun getWikiPages(gscoord: String) =
        api.getNearbyWikiPages(gscoord = gscoord)

    fun getWikiPageDetails(pageId: String) =
        api.getWikiPageDetails(pageid = pageId)

    fun getImageUrl(title: String) =
        api.getImageUrl(titles = title)

}