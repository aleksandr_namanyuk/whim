package fi.maas.whim.repository

import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import fi.maas.whim.BuildConfig.DIRECTIONS_API_URL
import fi.maas.whim.models.GoogleDirectionResponse
import io.reactivex.Observable
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request

class GoogleDirectionRepository(
    private val apiKey: String,
    private val okHttpClient: OkHttpClient,
    private val gson: Gson
): IGoogleDirectionRepository {

    override fun getDirections(
        origin: LatLng,
        destination: LatLng
    ): Observable<GoogleDirectionResponse> = Observable.create<GoogleDirectionResponse> { emitter ->
        try {

            HttpUrl.parse(DIRECTIONS_API_URL)?.newBuilder()
                ?.addPathSegment("json")
                ?.addQueryParameter("origin", "${origin.latitude},${origin.longitude}")
                ?.addQueryParameter("destination", "${destination.latitude},${destination.longitude}")
                ?.addQueryParameter("key", apiKey)
                ?.addQueryParameter("mode", "walking")
                ?.build()
                ?.toString()
                ?.let {

                    val request = Request.Builder()
                        .url(it)
                        .build()

                    val response = okHttpClient.newCall(request).execute()

                    val googleDirections = gson.fromJson(
                        response.body()?.string(),
                        GoogleDirectionResponse::class.java
                    )

                    emitter.onNext(googleDirections)

                }

        } catch (e: Throwable) {

            emitter.onError(e)
        }

        emitter.onComplete()
    }

}

interface IGoogleDirectionRepository{

    fun getDirections(
        origin: LatLng,
        destination: LatLng
    ): Observable<GoogleDirectionResponse>

}