package fi.maas.whim.viewmodels

import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import fi.maas.whim.repository.GoogleDirectionRepository
import fi.maas.whim.repository.IGoogleDirectionRepository
import fi.maas.whim.repository.WikiPageRepository

class MainViewModel(
    private val wikiRepository: WikiPageRepository,
    private val googleRepository: IGoogleDirectionRepository
) : ViewModel(){

    /**
     * Rx solutions, but in MVVM model architecture. Even though for Rx is better to use MVP model.
     */

    fun getNearbyWikiPages(gscoord: String) =
        wikiRepository.getWikiPages(gscoord)

    fun getWikiPageDetails(pageId: String) =
        wikiRepository.getWikiPageDetails(pageId)

    fun getImageUrl(title: String) =
        wikiRepository.getImageUrl(title)

    fun getRoutes(
        origin: LatLng,
        destination: LatLng) = googleRepository.getDirections(origin, destination)


}