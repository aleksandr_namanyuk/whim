package fi.maas.whim.viewmodels

import androidx.lifecycle.ViewModel
import fi.maas.whim.repository.WikiPageRepository

class MarkerDetailsViewModel(private val wikiPageRepository: WikiPageRepository) : ViewModel(){

    fun getWikiPageDetails(pageId: String) =
        wikiPageRepository.getWikiPageDetails(pageId)


}