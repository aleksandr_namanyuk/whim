package fi.maas.whim.utils

import fi.maas.whim.models.WikiImage
import java.lang.StringBuilder

class ImagesRequestUtils {

    fun createTitlesParam(images: List<WikiImage>,
                          imagesMaxSize: Int = 10): String {
        val titles = StringBuilder()
        images.forEachIndexed { index, wikiImage ->

            if(index == imagesMaxSize)
                return@forEachIndexed

            titles.append(wikiImage.title)

            if(index != images.size - 1)
                titles.append("|")

        }
        return titles.toString()
    }

}