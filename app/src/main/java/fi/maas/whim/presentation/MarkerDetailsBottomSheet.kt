package fi.maas.whim.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import fi.maas.whim.R
import fi.maas.whim.viewmodels.MarkerDetailsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber


class MarkerDetailsBottomSheet : BottomSheetDialogFragment(){

    private val markerDetailsViewModel: MarkerDetailsViewModel by viewModel()

    private val compositeDisposable = CompositeDisposable()

    private lateinit var titleTv: TextView
    private lateinit var descriptionTv: TextView

    companion object{

        const val KEY_PAGE_ID = "key_page_id"

        fun newInstance(pageId: String) = MarkerDetailsBottomSheet().also {
            val bundle = Bundle()
            bundle.putString(KEY_PAGE_ID, pageId)
            it.arguments = bundle
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val pageId = arguments?.getString(KEY_PAGE_ID)
        Timber.v("pageId : $pageId")

        val view = inflater.inflate(R.layout.bottom_sheet_marker_details, container, false)
        titleTv = view.findViewById(R.id.poi_title)
        descriptionTv = view.findViewById(R.id.poi_description)

//        val bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById<CoordinatorLayout>(R.id.bottom_sheet_layout))
//        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//
//        bottomSheetBehavior.setBottomSheetCallback(
//            object : BottomSheetBehavior.BottomSheetCallback() {
//
//                override fun onStateChanged(bottomSheet: View, newState: Int) {
//                }
//
//                override fun onSlide(bottomSheet: View, slideOffset: Float) {
//                }
//        })

        pageId ?: return view

        subscribeToWikiPageDetails(pageId)

        Timber.v("pageId : $pageId")

        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun subscribeToWikiPageDetails(pageId: String){
        val dis = markerDetailsViewModel.getWikiPageDetails(pageId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                val wikiPage = it.query.pages[pageId]

                titleTv.text = wikiPage?.title
                descriptionTv.text = wikiPage?.description

                Timber.v("title : ${wikiPage?.title}")

            },{
                Timber.e(it)
            })

        compositeDisposable.add(dis)
    }


}