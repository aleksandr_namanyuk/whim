package fi.maas.whim.presentation.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fi.maas.whim.R
import kotlinx.android.synthetic.main.image_item.view.*

class ImageAdapter(private val items : ArrayList<String>) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.image_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(holder.imageView.context)
            .load(items[position])
            .error(R.drawable.ic_launcher_foreground)
            .into(holder.imageView)
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

    val imageView: ImageView = view.img_wiki_photo

}