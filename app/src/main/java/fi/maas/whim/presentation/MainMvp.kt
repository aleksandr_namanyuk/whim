package fi.maas.whim.presentation

import android.graphics.Color
import com.google.android.gms.maps.model.Dot
import com.google.android.gms.maps.model.Gap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import fi.maas.whim.core.BasePresenter
import fi.maas.whim.core.IView
import fi.maas.whim.models.PageDetails
import fi.maas.whim.models.Polyline
import fi.maas.whim.models.WikiImage
import fi.maas.whim.models.WikiPage
import fi.maas.whim.presentation.details.ImageAdapter
import fi.maas.whim.repository.IGoogleDirectionRepository
import fi.maas.whim.repository.WikiPageRepository
import fi.maas.whim.utils.ImagesRequestUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class MainPresenter(
    private val wikiRepository: WikiPageRepository,
    private val googleRepository: IGoogleDirectionRepository,
    private val imagesRequestUtils: ImagesRequestUtils
): BasePresenter<MainView>(){

    fun getNearbyWikiPages(gscoord: String) =
        wikiRepository.getWikiPages(gscoord)

    fun getWikiPageDetails(pageId: String) =
        wikiRepository.getWikiPageDetails(pageId)

    fun getImageUrl(title: String) =
        wikiRepository.getImageUrl(title)

    fun getRoutes(
        origin: LatLng,
        destination: LatLng
    ) = googleRepository.getDirections(origin, destination)

    fun subscribeToWikiPages(gscoord: String){
        val dis = getNearbyWikiPages(gscoord)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Timber.d("Wiki pages : ${it.query.geosearch.size}")

                view?.addMarkers(it.query.geosearch)


            }, {
                Timber.e(it)
            })

        disposable.add(dis)
    }

    fun onMarkerClick(wikiPageId: String){

        val dis = getWikiPageDetails(wikiPageId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ pageDetailsQuery ->

                val currentWikiPage = pageDetailsQuery.query.pages[wikiPageId]
                view?.updateBottomSheetDetails(currentWikiPage)

            },{
                Timber.e(it)
            })

        disposable.add(dis)
    }

    fun loadImagesToCarousel(images: List<WikiImage>){
        imagesRequestUtils.createTitlesParam(images).let {
            getImageUrl(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { imageInfoQuery ->

                        val urlList: ArrayList<String> = arrayListOf()
                        imageInfoQuery.query.pages.values.forEach { imageInfoPage ->
                            val url = imageInfoPage.imageinfo.first().url
                            Timber.v("imageInfo url : $url")
                            urlList.add(url)
                        }

                        view?.addImages(urlList)

                    },
                    { e ->
                        Timber.w(e)
                    }
                )
        }
    }

    fun onGetThereClick(origin: LatLng,
                        destination: LatLng){
        val dis = getRoutes(origin, destination)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ googleDirectionsResponse ->

                Timber.v("onSuccess ${googleDirectionsResponse.status}")

                googleDirectionsResponse.routes.forEach { route ->

                    val p = PolylineOptions()
                        .pattern(listOf( Dot(), Gap(10f)))
                        .width(5f)
                        .color(Color.BLUE)

                    route.legs.forEach { leg ->
                        leg.steps.forEach { step ->
                            p.add(
                                LatLng(step.start_location.lat, step.start_location.lng),
                                LatLng(step.end_location.lat, step.end_location.lng)
                            )
                        }
                    }

                    view?.addDirection(p)

                }

            },
                {
                    Timber.v(it)
                },
                {
                    Timber.v("onComplete")
                })

        disposable.add(dis)
    }

}


interface MainView: IView{

    fun addMarkers(pages: List<WikiPage>)

    fun addImages(urlList: ArrayList<String>)

    fun addDirection(p: PolylineOptions)

    fun updateBottomSheetDetails(pageDetails: PageDetails?)
}