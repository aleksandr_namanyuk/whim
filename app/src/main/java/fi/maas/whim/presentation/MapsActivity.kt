package fi.maas.whim.presentation

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.tbruyelle.rxpermissions2.RxPermissions
import fi.maas.whim.R
import fi.maas.whim.models.Coordinate
import fi.maas.whim.models.PageDetails
import fi.maas.whim.models.WikiImage
import fi.maas.whim.models.WikiPage
import fi.maas.whim.presentation.details.ImageAdapter
import fi.maas.whim.utils.ImagesRequestUtils
import fi.maas.whim.viewmodels.MainViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.bottom_sheet_marker_details.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, MainView {

    private var mMap: GoogleMap? = null

    /**
     * MVVM solution
     */
//    private val mainViewModel: MainViewModel by viewModel()


    /**
     * MVP solution
     */
    private val mainPresenter: MainPresenter by inject()

    private var locationManager : LocationManager? = null

    private val compositeDisposable = CompositeDisposable()

    private var detailsBottomSheet: MarkerDetailsBottomSheet? = null

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        mainPresenter.attach(this)

        //RecyclerView for image carousel
        image_list.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        bottomSheetBehavior = BottomSheetBehavior.from(marker_details_bottom_layout)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
        detailsBottomSheet?.onDestroy()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap?.uiSettings?.apply {
            isZoomControlsEnabled = true
            isZoomGesturesEnabled = true
            isCompassEnabled = true
            isRotateGesturesEnabled = true
        }

        checkLocationPermission()
    }

    @SuppressLint("CheckResult")
    private fun checkLocationPermission(){
        RxPermissions(this)
            .request(Manifest.permission.ACCESS_FINE_LOCATION)
            .subscribe { granted ->
                if (granted) {

                    mMap?.isMyLocationEnabled = true

                    iniLocationManager()

                    getCurrentLocation()?.let { location ->
                        mainPresenter.subscribeToWikiPages("${location.latitude}|${location.longitude}")
                    }


                } else {
                    Timber.v("Location permission is denied!")

                    checkLocationPermission()

                    Toast.makeText(this, getString(R.string.location_denied), Toast.LENGTH_LONG)
                        .show()
                }
            }
    }

    private fun getCurrentLocation(): LatLng? {
        return try {
            val lastKnownLocation = locationManager?.getLastKnownLocation("gps")
            lastKnownLocation?.let { loc ->

                mMap?.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(loc.latitude, loc.longitude),
                        16f
                    )
                )

                Timber.v("current location : ${loc.latitude} ${loc.longitude}")

                return@let LatLng(loc.latitude, loc.longitude)
            }
        } catch (e: SecurityException){
            Timber.w("Security problem, no location available")
            return null
        }
    }

    override fun addMarkers(pages: List<WikiPage>) {

        pages.forEachIndexed { index, wikiPage ->

            mMap?.addMarker(
                MarkerOptions()
                    .position(
                        LatLng(wikiPage.lat, wikiPage.lon
                        )
                    )
                    .title(wikiPage.title)

            )?.tag = wikiPage

            Timber.v("${wikiPage.title} -- $index")
        }

        // Set a listener for marker click.
        mMap?.setOnMarkerClickListener(this)
    }

    private fun iniLocationManager(){
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?

        Timber.v("Provider : ")
        locationManager?.allProviders?.forEach {
            Timber.v(it)
        }

        try {

            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f,
                object : LocationListener {

                    override fun onLocationChanged(location: Location?) {
                        Timber.v("latitude: ${location?.latitude} & longitude: ${location?.longitude} ")

                        location?.let {
                            mMap?.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    LatLng(location.latitude, location.longitude),
                                    16f)
                            )
                        }

                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

                    override fun onProviderEnabled(provider: String?) {}

                    override fun onProviderDisabled(provider: String?) {}
                }
            )

        } catch (e: SecurityException) {
            Timber.w("Security problem, no location available")
        }
    }

    fun createLocationObservable(context: Context) =
        Observable.create<Location> { emitter ->

            if(ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                emitter.onError(SecurityException("No location permission"))
                return@create
            }

        }

    override fun onMarkerClick(p0: Marker?): Boolean {

        val wikiPage = p0?.tag as WikiPage
        val wikiPageId = wikiPage.pageId
        Timber.v("marker tag: $wikiPageId")

        mainPresenter.onMarkerClick(wikiPageId)

        return false
    }

    override fun addImages(urlList: ArrayList<String>) {
        image_list.adapter = ImageAdapter(urlList)
    }

    private fun onGetThereClick(coordinate: Coordinate?){
        coordinate?.let { destination ->
            getCurrentLocation()?.let { origin ->

                mainPresenter.onGetThereClick(
                    LatLng(origin.latitude, origin.longitude),
                    LatLng(destination.lat, destination.lon)
                )
            }

        }
    }

    override fun addDirection(p: PolylineOptions) {
        mMap?.addPolyline(p)
    }

    override fun updateBottomSheetDetails(pageDetails: PageDetails?) {

        poi_title.text = pageDetails?.title
        poi_description.text = pageDetails?.description

        pageDetails?.images?.let {
            mainPresenter.loadImagesToCarousel(it)
        }

        val coordinate = pageDetails?.coordinates?.firstOrNull()
        Timber.v("coordinates: ${coordinate?.lat}, ${coordinate?.lon}")

        poi_get_there.setOnClickListener{
            onGetThereClick(coordinate)
        }

        updateBottomSheetBehaviour()
    }

    private fun updateBottomSheetBehaviour() = with(bottomSheetBehavior){
            state = if(state != BottomSheetBehavior.STATE_EXPANDED) {
                BottomSheetBehavior.STATE_HALF_EXPANDED
            } else {
                poi_title.text = ""
                poi_description.text = ""
                BottomSheetBehavior.STATE_COLLAPSED
            }
        }



}
