package fi.maas.whim.core.log

import fi.maas.whim.BuildConfig
import timber.log.Timber

class WhimLog {

    companion object {

        fun init(){
            if(BuildConfig.DEBUG){
                Timber.plant(DebugTree())
            } else {
                Timber.plant(ReleaseTree())
            }
        }
    }

}