package fi.maas.whim.core.log

import android.util.Log
import timber.log.Timber

class ReleaseTree: Timber.Tree(){

    override fun isLoggable(tag: String?, priority: Int): Boolean {
        return when(priority){
            Log.ERROR, Log.ASSERT -> true
            else -> false
        }
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {

        if(!isLoggable(tag, priority))
            return

        when(priority){
            Log.ERROR -> {}
            Log.ASSERT -> {}
        }
    }
}