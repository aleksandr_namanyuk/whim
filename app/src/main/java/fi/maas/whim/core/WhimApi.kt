package fi.maas.whim.core

import fi.maas.whim.models.GeosearchQuery
import fi.maas.whim.models.ImageInfoQuery
import fi.maas.whim.models.PageDetailsQuery
import fi.maas.whim.models.QueryRequest
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WhimApi{

    @GET("api.php?")
    fun getWikiPageDetails(
        @Query("action") action: String? = "query",
        @Query("prop") prop: String? = "info|description|images|coordinates",
        @Query("pageids") pageid: String,
        @Query("format") format: String = "json"
    ): Single<QueryRequest<PageDetailsQuery>>

    @GET("api.php?")
    fun getNearbyWikiPages(
        @Query("action") action: String? = "query",
        @Query("list") list: String? = "geosearch",
        @Query("gsradius") gsradius: String? = "10000",
        @Query("gscoord") gscoord: String,
        @Query("gslimit") gslimit: String? = "50",
        @Query("format") format: String = "json"
    ): Single<QueryRequest<GeosearchQuery>>

    @GET("api.php?")
    fun getImageUrl(
        @Query("action") action: String? = "query",
        @Query("titles") titles: String,
        @Query("prop") prop: String = "imageinfo",
        @Query("iiprop") iiprop: String = "url",
        @Query("format") format: String = "json"
    ): Single<QueryRequest<ImageInfoQuery>>
}