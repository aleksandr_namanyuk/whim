package fi.maas.whim.core

import io.reactivex.disposables.CompositeDisposable

/** MVP classes */

interface IView

interface IPresenter<V: IView>{

    fun attach(view: V)

    fun detach()

    fun destroy()
}

open class BasePresenter<V: IView> : IPresenter<V>{

    val disposable: CompositeDisposable = CompositeDisposable()

    var view: V? = null

    override fun attach(view: V) {
        this.view = view
    }

    override fun detach() {

        disposable.clear()
        view = null
    }

    override fun destroy() {
        if(!disposable.isDisposed)
            disposable.dispose()
    }
}